# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 22:25:12 2019

@author: jordan
"""
import csv
 
state = []
breach = []
organ = []
X = []

with open('./Privacy_Rights_Clearinghouse-Data-Breaches-Export.csv') as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    labels=next(csvReader) # grab and skip over the row labels on the first line
    for row in csvReader:
        state.append(row[3])       # State
        breach.append(row[4])      # Type of Breach
        organ.append(row[5])       # Type of Organization
        X.append([row[3],row[5]])  # combined state and organ
#print(organ)
#print(breach)
#print(labels)

from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(handle_unknown='ignore')
print(X)
enc.fit(X)
print("Categories are:")
print(enc.categories_)
print("Feature names are:")
print(enc.get_feature_names())
print("Example tranformation encoding for:")
example = [['Tennessee','MED'], ['Kentucky','EDU']]
print(example)
print("  is...")
print(enc.transform(example).toarray())
print("All original two column data converted into encoded byte-array:")
# Output our two column data converted into a single encoded byte-array
print(enc.transform(X).toarray())
print(breach)

# Put breach values into proper format for Baysian
from statsmodels.tools import categorical
import numpy as np
a = np.array(breach)              # raw values e.g. DISC, PORT, ...
b = categorical(a, drop=True)     # bytearray e.g [0,1]
c = b.argmax(1)                   # values 0,1,2,3

# Do Bayes Here
X2 = enc.transform(X).toarray()
Y = c
from sklearn.naive_bayes import BernoulliNB
# Fit Bernoulli NB w/ alpha = 1.0
clf = BernoulliNB(alpha=1.0)
# make a dummies for 'a' using pandas so it becomes binary encoded
import pandas as pd
Ybin = pd.get_dummies(a)

clf.fit(X2, Y)

print('Score: {0}'.format(clf.score(X2,Y)))
#print('Training Score: {0}'.format(cls.score(X_train,y_train)))
#print('Testing Score: {0}'.format(cls.score(X_test, y_test))) 

 # Manually form the (log) numerator and denominator that
# constitute P(feature presence | class)
num = np.log(clf.feature_count_ + 1.0)
denom = np.tile(np.log(clf.class_count_ + 2.0), (X2.shape[1], 1)).T

# Check manual estimate matches
np.testing.assert_array_equal(clf.feature_log_prob_, (num - denom)) 

#
#from sklearn.preprocessing import OneHotEncoder
#enc = OneHotEncoder(handle_unknown='ignore')
#print(X)
#enc.fit(X)
#print(enc.categories_)
#print(enc.get_feature_names())
#print(enc.transform([['PORT', 'MED'], ['DISC', 'EDU']]).toarray())
## Output our two column data converted into a single encoded byte-array
#print(enc.transform(X).toarray())











