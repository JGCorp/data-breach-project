DataBreach File Descriptions: 

------Onehotencoding.py and ordinalEncoding.py----

These two python files contain the process of encoding the categorical values in the dataset into numerical values, part of the project analysis included seeing if encoding in a different manner may change the performance of the algorithm. These files also include running both NB and Random Forest in order to predict the type of breach a particular organization is most likely to endure. *Based on particular features*

----Privacy_Rights_Clearinghouse CSV file------

This was the data the project was based off of, information collected included data breach events occuring over the past 11 years in the United States, a description of the incident, the type of hack that occured, the location of the organization, and the type of organization.

------SCPowerPoint.pptx---------

A short powerpoint presentation describing the steps of the project 

------ convertData.R and allLocationDensity.csv-------

 Using the location data, the population density was able to be automatically found for each breach location and mapped into an additional field. This was done using an R library called RDSTK. Once this was done, the final product was stored into allLocationDensity.csv

 -------global.R/server.R/ui.R-------

 Shiny application that was able to take numerical fields and visualize them into a clustering graph as well as boxplots. (Used to explore)